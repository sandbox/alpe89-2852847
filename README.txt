This is a very simple module that does only one thing:
Disables the option "Delete the account and its content."
when cancelling a user account.
